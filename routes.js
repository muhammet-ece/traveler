'use strict';

var express = require('express');
var router = express.Router();

module.exports = function () {
    router = require('./core/routes/routes.route')(router);
    return router;
};
