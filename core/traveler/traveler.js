/*jshint esversion: 6 */
"use strict";

const schemaName = 'traveler';

module.exports = function (mongoose) {
    var schema = mongoose.Schema(
        {
            name: {type: String},
            lastLocation: {type: Array}
        },
        {
            toObject: {virtuals: true},
            toJSON: {virtuals: true},
            versionKey: false,
            timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' }
        }
    );

    return mongoose.model(schemaName, schema);
};

