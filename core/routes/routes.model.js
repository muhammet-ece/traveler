/*jshint esversion: 6 */
'use strict';

var Routes = require('../database.js').Routes;

exports.findByQuery = async (query, sort/*, populate*/) => {
    try {
        // populate works but before populate we should update lastLocation for traveler collection.
        // Otherwise, it will always show first data that we added manually.
        const result = await Routes.find(query).sort(sort).select('-updatedAt')//.populate(populate)
        return {success:true, result:result}

    } catch (error) {
        return {success:false, error:error}
    }
}

exports.create = async (params) => {
    try {
        params.lastLocation = params.coordinates;
        const routes = new Routes(params);
        const route = await routes.save();
        return({success:true,result:route})
    } catch (error) {
        return({success:false, error:"create error"});
    }
};


