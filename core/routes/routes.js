/*jshint esversion: 6 */
"use strict";

const schemaName = 'route';

module.exports = function (mongoose) {
    var schema = mongoose.Schema(
        {
            travelerId: {type:mongoose.Schema.ObjectId, ref:'traveler'},
            lastLocation: {type: Array}
        },
        {
            toObject: {virtuals: true},
            toJSON: {virtuals: true},
            versionKey: false,
            timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' }
        }
    );

    return mongoose.model(schemaName, schema);
};

