'use strict';

const express = require('express');
const router = express.Router();

const controller = require('./routes.controller');


module.exports = function (router) {
    //API's
    router.route('/summary')
        .get(controller.dailyList)

    router.route('/traveler/route/')
        .get(controller.getLastLocation);

    router.route('/coordinate')
        .post(controller.create)

    return router;
};
