const model = require('./routes.model');
exports.getLastLocation = async (travelerId) => {
    try {
        const response = await model.findByQuery({travelerId:travelerId},{createdAt:-1});
        if(response.result.length > 0){
            return{success:true,result:response.result[0],status:200};

        } else {
            return {success:false, result:[], status:404, error:"No item to show"}
        }
    } catch (error) {
        return{success:false, status:500, error:"cannot get last location"};
    }

};

exports.dailyList = async (travelerId) => {
    try {
        const response = await model.findByQuery({$and:[{travelerId:travelerId},{"createdAt":{$gt:new Date(Date.now() - 24*60*60 * 1000)}}]},{createdAt:-1});
        if(response.result.length > 0){
            return{success:true,result:response.result,status:200};
        } else {
            return {success:false, result:[], status:404, error:"No item to show"}
        }

    } catch (error) {
        return{success:false, status:500, error:"cannot get last location"};
    }
}