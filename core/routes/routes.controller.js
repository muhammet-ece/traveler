const service = require('./routes.service');
const model = require('./routes.model')

exports.getLastLocation = async (req, res, next) => {
    try {
        console.log(req.query);
        const response = await service.getLastLocation(req.query.TRAVELER_ID)
        if(response.success){
            res.send({success:true,result:response.result,status:response.status});
        }else{
            res.send({success:false,error:response.error,status:response.status});
        }
    } catch (error) {
        res.send({success:false,error:error})
    }
};

exports.create = async (req, res) => {
    try {
        const response = await model.create(req.body);
        res.send({success:true,status:response.status})
    } catch (error) {
        res.send({success:false,error:error})
    }
}

exports.dailyList = async (req,res) => {
    try{
        const response = await service.dailyList(req.query.TRAVELER_ID);
        if(response.success) {
            res.send({success: true, result: response.result, status: response.status})
        } else {
            res.send({success:false,error:response.error,status:response.status});
        }
    } catch (error) {
        res.send({success:false,error:error})
    }
}